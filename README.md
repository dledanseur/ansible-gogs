# Purpose 
This module allows installing the Gogs git repository server, backed with a mysql database. 

# Dependencies 
This module depends on the dledanseur.mysql module (https://bitbucket.org/dledanseur/ansible-mysql) 

# Variables 
The following variable are available: 
* gogs_mysql_database: name of the database (defaults to gogs)
* gogs_mysql_user: name of the user connecting to the database (defaults to gogs)
* gogs_mysql_password: password of the mysql user connecting to the database
* gogs_repository_root: Path to the root of the gir repostories
* gogs_root_url: External http url to access the server
